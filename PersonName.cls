public class PersonName {
    public String firstName {get;set;}
    public String lastName {get;set;}
    public PersonName(String fName){
        firstName = fName;
        lastName = 'unknown';
    }
     public PersonName(String fName, String lName){
        firstName = fName;
        lastName = lName;
    }
    public String getFullName() {
        return firstName+' '+lastName;
    }
}
