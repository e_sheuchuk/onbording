@isTest
public class listPractices_Test {
    static TestMethod void getLastListItem_Test() {
        List<String> expectedResult = new String[]{'first','second'};
        List<String> actualResult = (List<String>)listPractices.getLastListItem(new String[]{'first','second', 'last'},2); 
        System.assertEquals(expectedResult, actualResult, 'The two list are not equal!');
    }
   static TestMethod void addElementToEnd_Test(){
        List<String> expectedResult_1 = new String[]{'first','second','third'};
        List<Object> expectedResult_2 = new String[]{'third'};
        List<String> actualResult_1 = (List<String>)listPractices.addElementToEnd(new String[]{'first','second'},'third',1);
        List<Object> actualResult_2 = listPractices.addElementToEnd(new String[]{'first','second'},'third',2);
        System.assertEquals(expectedResult_1, actualResult_1,'addElementToEnd_Test_Mode1:The two list are not equal!');
        System.assertEquals(expectedResult_2, actualResult_2,'addElementToEnd_Test_Mode2:The two list are not equal!');
   }
   static TestMethod void createList_Test(){
       List<Integer> expectedResult = new Integer[]{1,2,3,4,5,6,7,8,9,10};
       List<Integer> actualResult = listPractices.createList(1,10);
       System.assertEquals(expectedResult, actualResult,'createList: The two list are not equal!');
   }
   static TestMethod void countListItems_Test(){
        Integer actualResult = listPractices.countListItems(listPractices.createList(1,10),9);
        Integer expectedResult = 19;
        System.assertEquals(expectedResult, actualResult , 'The result is not equal!');
        Integer expectedResult_2 = 0;
        integer actualResult_2 = listPractices.countListItems(listPractices.createList(1,10),11);
        System.assertEquals(expectedResult_2, actualResult_2 , 'The result is not equal! index = 11, List.size()=10 => result must be 0 because list size = 10');
   }
   static TestMethod void bubbleSort_Test(){
       List<Integer> expectedResult = new Integer[]{0, 1, 2, 3, 4, 6, 12};
       List<Integer> actualResult = listPractices.bubbleSort(new Integer[]{4, 1, 3, 6, 12, 2, 0});
       System.assertEquals(expectedResult, actualResult, 'bubbleSort: array wrong sort!');
   }
   static TestMethod void binarySearch_Test(){
        Integer expectedResult = 4;//Integer[]{2,3,4,6,7,9};
        Integer actualResult = listPractices.binarySearch(listPractices.bubbleSort(new Integer[]{4,9,7,6,2,3}), 7);
        System.assertEquals(expectedResult,actualResult,'binarySearch: wrong index of element!');
   }
   static TestMethod void isSymmetricMatrix_Test(){
        List<List<Integer>> matrix_true = new List<List<Integer>>();
        matrix_true.add(new Integer[]{1,3,0});
        matrix_true.add(new Integer[]{3,2,6});
        matrix_true.add(new Integer[]{0,6,5});
        List<List<Integer>> matrix_false = new List<List<Integer>>();
        matrix_false.add(new Integer[]{1,3,0});
        matrix_false.add(new Integer[]{3,2,10});
        matrix_false.add(new Integer[]{0,6,5});
        Boolean actualResult_true = listPractices.isSymmetricMatrix(matrix_true);
        Boolean actualResult_false = listPractices.isSymmetricMatrix(matrix_false);
        System.assertEquals(true,actualResult_true, 'isSymmetricMatrix: expectet result is TRUE but it was FALSE!');
        System.assertEquals(false,actualResult_false, 'isSymmetricMatrix: expectet result is FALSE but it was TRUE!');
   }
}