public class Human extends Monkey{
    public Human() {
        super('Human');
    }
    public Human(String setName){
        super(setName);
    }
    public String speak() {
        return Name+' speak';
    }
    public String think() {
        return Name+' think';
    }
}
